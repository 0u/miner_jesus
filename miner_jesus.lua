-- Miner jesus v0.1 (for opencomputers)

local internet = require('internet')
local r, coords = require('coords')

----------------------- Internet -----------------------
local server_ip = function()
  local buf = ''
  for chunk in internet.request("https://ipecho.net/plain") do
    buf = buf .. chunk
  end
  -- trim whitespace
  return buf:gsub("^%s*(.-)%s*$", "%1")
end

-- Connect to the server on a specific port
local connect = function(port)
  return internet.socket(server_ip(), port)
end

------------------------- Robot ------------------------

local checkFuel = function()
end

-- go forward n times, breaking anything in your way.
-- if n is nil then go forward once.
local forward = function(n)
  for i=1,n or 1 do
    while not r.forward() do
      checkFuel()
      while r.swing() do end
    end
  end
end
